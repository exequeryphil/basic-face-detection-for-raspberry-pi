## Basic face detection for Raspberry Pi

Uses Python 3, OpenCV, and a webcam.

Note that a `requirements.txt` is included.

Also, you may have to run:
```
sudo apt-get install libatlas-base-dev libjasper-dev libqtgui4 libqt4-test
```
